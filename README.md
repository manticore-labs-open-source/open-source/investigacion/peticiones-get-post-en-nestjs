# Peticiones Get Post en nestjs

En esta ocasion aprenderemos como preparar un servidor nest para peticiones get y post.

Utilizaremos el proyecto anterior que instalamos en el siguiente [tutorial](https://gitlab.com/manticore-labs/open-source/investigacion/levantar-servidor-nestjs).

Debido a que nestjs es muy similar a Angular debemos declarar nuestras peticiones en un archivo controller.
en este caso utilizaremos el archivo app.controller para manejar las peticiones.

Al acceder a este archivo veremos el siguiente codigo que viene predeterminado.
![](imagenes/1controller.png)

Este archivo junto con el servicio imprimen el "Hello World" que vimos en la entrada anterior.

Como se ve en la imagen los metodos GET se declaran mediante el decorador:
```TypeScript
@Get()
```

seguido de un método de la siguiente manera:
``` TypeScript
@Get()
funcionReaccionarGet(){
    return "Lo que sea" 
}
```
si deseamos que el GET tenga una ruta le asignamos dentro de los paréntesis del decorador:

``` TypeScript
@Get('respuesta')
funcionReaccionarGet(){
    return "Lo que sea" 
}
```

podremos ver en el navegador que nuestro código funciona. Levantamos el servidor con 
```
npm run start
```
y vemos que nuestro get se encuentra mapeado cuando se inicia el servidor
![](imagenes/respuesta.png)

y haciendo la petición desde nuestro navegador podemos observar que en efecto funciona.

![](imagenes/loquesea.png)

Lo mismo sucede con el método post, justo debajo del método get escribimos lo siguiente

``` TypeScript
@Post()
metodoPost(){

}
```
si deseamos que el método POST tenga una ruta la escribimos entre los paréntesis del decorador.
``` TypeScript
@Post('enviar-data')
metodoPost(){

}
```
Si enviamos body params debemos utilizar el decorador Body para poder recuperarlos de esta manera

``` TypeScript
@Post('enviar-data')
metodoPost(
    @Body('user') user
){
    console.log(user)
}
```
En el código anterior vamos a recuperar un parametro body llamado *user* y lo vamos a imprimir

Reiniciamos el servidor y observamos que el método post ya se encuentra mapeado
![](imagenes/post1.png)

Probamos el método POST en el programa **Postman** 
![](imagenes/postman.png)
desde aquí enviamos el parámetro y como no retornamos nada en no nos muestra nada en la respuesta. Pero en el lado del servidor si cumple con la función como podemos ver en la siguiente captura:
![](imagenes/post2.png)

Así de fácil se crean los métodos GET y POST en nestjs para siguientes entradas estudiaremos la distribución de los archivos.

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>


