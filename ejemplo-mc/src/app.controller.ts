import { Get, Controller, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('respuesta')
  funcionReaccionarGet() {
    return "Lo que sea"
  }

  @Post('enviar-data')
  metodoPost(
    @Body('user') user
  ) {
    console.log(user)
  }
}
